<?php
    /* подключаем файл, в котором получаем пользователей из бд */
    include 'get_users.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>
        Тестовое задание
    </title>
    <meta name="description" content="Chartist.html">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
    <link id="vendorsbundle" rel="stylesheet" media="screen, print" href="css/vendors.bundle.css">
    <link id="appbundle" rel="stylesheet" media="screen, print" href="css/app.bundle.css">
    <link id="myskin" rel="stylesheet" media="screen, print" href="css/skins/skin-master.css">
    <link rel="stylesheet" media="screen, print" href="css/statistics/chartist/chartist.css">
    <link rel="stylesheet" media="screen, print" href="css/miscellaneous/lightgallery/lightgallery.bundle.css">
    <link rel="stylesheet" media="screen, print" href="css/fa-solid.css">
    <link rel="stylesheet" media="screen, print" href="css/fa-brands.css">
    <link rel="stylesheet" media="screen, print" href="css/fa-regular.css">
</head>
<body class="mod-bg-1 mod-nav-link ">
<main id="js-page-content" role="main" class="page-content">

    <div class="col-md-6">
        <div id="panel-1" class="panel">
            <div class="panel-container show">
                <div class="panel-content">
                    <div class="panel-content">
                        <div class="form-group">
                            <form action="handler_add.php" method="post">
                                <label class="form-label" for="simpleinput">Добавить нового пользователя</label>
                                <input name="login" type="text" id="simpleinput" class="form-control">
                                <button type="submit" class="btn btn-success mt-3">Добавить</button>
                            </form>
                        </div>
                    </div>

                    <? if($people): ?>
                        <div class="frame-heading">
                            Список имеющихся пользователей
                        </div>
                        <div class="frame-wrap">
                            <table class="table m-0">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Логин</th>
                                    <th>Возможные действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                <? foreach ($people as $key => $person):?>
                                    <tr>
                                        <th scope="row"><?= ++$key; ?></th>
                                        <td><?= $person['login']; ?></td>
                                        <td>
                                            <div onclick="deletePerson('<?= $person['id'] ?>', '<?= $person['login'] ?>')" class="btn btn-danger">Удалить</div>
                                        </td>
                                    </tr>
                                <? endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    <? else: ?>
                        <div>Активных пользователей на данный момент нет</div>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script src="js/script.js"></script>
</body>
</html>
