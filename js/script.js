function deletePerson(userId, username) {
    let values = [];
    values.push('userId=' + userId);
    values.push('username=' + username);
    let fields = values.join('&');

    fetch('handler_delete.php', {
        method: 'POST',
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        },
        body: fields,
    })
    .then(response => {
        if (response.status !== 200) {
            return Promise.reject();
        }
    })
    .then(info => {
        setTimeout(
            location.href=location.href,
            5000);
    })
}